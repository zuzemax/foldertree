package mypackage;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        List<Folder> list = new ArrayList<>();
        list.add(new Folder(1, null, "bin"));
        list.add(new Folder(2, null, "home"));
        list.add(new Folder(3, null, "opt"));
        list.add(new Folder(4, 2, "user"));
        list.add(new Folder(5, 4, "downloads"));
        list.add(new Folder(6, 4, "documents"));
        list.add(new Folder(7, 4, "pictures"));
        list.add(new Folder(8, 4, "projects"));
        list.add(new Folder(9, 8, "course_project"));
        list.add(new Folder(10, 8, "IS"));
        list.add(new Folder(11, 10, "webapp"));
        list.add(new Folder(12, 10, "test"));
        list.add(new Folder(13, 10, "untitled"));
        list.add(new Folder(14, 3, "tomcat"));
        list.add(new Folder(15, 14, "bin"));
        list.add(new Folder(16, 14, "conf"));
        list.add(new Folder(17, 14, "lib"));
        list.add(new Folder(18, 14, "logs"));
        list.add(new Folder(19, 14, "temp"));
        list.add(new Folder(20, 14, "webapps"));
        list.add(new Folder(21, 20, "docs"));
        list.add(new Folder(22, 20, "examples"));
        list.add(new Folder(23, 20, "host-manager"));
        list.add(new Folder(24, 20, "manager"));
        list.add(new Folder(25, 20, "ROOT"));
        list.add(new Folder(26, 16, "Catalina"));
        print(Folders.makeFoldersTree(list));
        print(Folders.makeFoldersTreeSecond(list));
        System.out.println(Folders.makeFoldersTree(list).equals(Folders.makeFoldersTreeSecond(list)));
    }

    static String spaces = "";

    static void print(Collection<Folder> folders) {
        for (Folder folder : folders) {
            System.out.println(spaces + folder.getName());
            spaces = spaces + "--";
            print(folder.getChildren());
            spaces = spaces.substring(0, spaces.length() - 2);
        }
    }
}