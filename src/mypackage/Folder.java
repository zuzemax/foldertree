package mypackage;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Objects;

public class Folder {
    private int id;
    private Integer parentId;
    private String name;
    private Collection<Folder> children = new LinkedList<>();

    public Folder(int id, Integer parentId, String name) {
        this.id = id;
        this.parentId = parentId;
        this.name = name;
    }

    @Override
    public String toString() {
        return "'" + name + "'" + children;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Folder folder = (Folder) o;
        return id == folder.id &&
                Objects.equals(children, folder.children);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, children);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<Folder> getChildren() {
        return children;
    }

    public void setChildren(Collection<Folder> children) {
        this.children = children;
    }
}
