package mypackage;

import java.util.*;

public class Folders {

    public static Collection<Folder> makeFoldersTree(Collection<Folder> folders) {
        Map<Integer, Folder> folderMap = new HashMap<>();
        folders.forEach(folder -> folderMap.put(folder.getId(), folder));
        Collection<Folder> foldersTree = new LinkedList<>();
        for (Folder folder : folderMap.values()) {
            if (folder.getParentId() != null) {
                folderMap.get(folder.getParentId()).getChildren().add(folder);
            } else {
                foldersTree.add(folder);
            }
        }
        return foldersTree;
    }

    public static Collection<Folder> makeFoldersTreeSecond(Collection<Folder> folders) {
        Map<Integer, Collection<Folder>> map = new HashMap<>();
        for (Folder folder : folders) {
            if (map.containsKey(folder.getId())) {
                folder.setChildren(map.get(folder.getId()));
            } else {
                map.put(folder.getId(), folder.getChildren());
            }
            if (!map.containsKey(folder.getParentId())) {
                map.put(folder.getParentId(), new ArrayList<>());
            }
            map.get(folder.getParentId()).add(folder);
        }
        return map.get(null);
    }
}
